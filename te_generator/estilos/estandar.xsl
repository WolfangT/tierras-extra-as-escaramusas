<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param name="base" />

    <xsl:template match="/">
        <html lang="es">
            <head>
                <meta charset="UTF-8" />
                <base href="FILE://{$base}/"/>
                <link rel="stylesheet" type="text/css" href="estilos/estandar.css" />
                <title>
                    <xsl:value-of select="carta/nombre" />
                </title>
            </head>
            <body>
                <xsl:apply-templates select="carta" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="carta">
        <div class="fondo">
            <xsl:apply-templates select="imagen" />
        </div>
        <div class="cara" tipo="{@tipo}" subtipo="{@subtipo}">
            <div class="tipo">
                <img src="iconos/nvl.png" alt="NVL" class="nvl" />
                <img src="iconos/{nivel}.png" alt="{nivel}" class="icono" />
            </div>
            <div class="nombre">
                <h1 class="titulo">
                    <xsl:value-of select="nombre" />
                </h1>
            </div>
            <div class="clases">
                <xsl:choose>
                    <xsl:when test="clases != ''">
                        <xsl:apply-templates select="clases" />
                    </xsl:when>
                    <xsl:otherwise>&#160;</xsl:otherwise>
                </xsl:choose>
            </div>
            <xsl:choose>
                <xsl:when test="@tipo = 'Recurso'">
                    <xsl:apply-templates select="recurso" />
                </xsl:when>
                <xsl:when test="@tipo = 'Personaje'">
                    <xsl:apply-templates select="atributos" />
                </xsl:when>
            </xsl:choose>

            <div class="inferior">
                <xsl:if test="habilidad != ''">
                    <xsl:apply-templates select="habilidad" />
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="@tipo = 'Habilidad'">
                        <p class="historiaLarga">
                            <xsl:value-of select="imagen/historia" />
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p class="historiaCorta">
                            <xsl:value-of select="imagen/historia" />
                        </p>
                    </xsl:otherwise>
                </xsl:choose>
            </div>

            <div class="pie">
                <xsl:apply-templates select="metadatos" />
            </div>
        </div>
    </xsl:template>

    <xsl:template match="clases">
        <xsl:apply-templates select="clase" />
    </xsl:template>

    <xsl:template match="clase">
        <div class="clase">
            <img src="iconos/{.}.png" alt="{.}" class="icono" />
        </div>
    </xsl:template>

    <xsl:template match="recurso">
        <div class="descripcion">
            <div class="tipo">
                <img class="icono" src="iconos/{@tipo}.png" />
            </div>
            <div class="valor">
                <img class="icono" src="iconos/{@valor}.png" alt="{@valor}" />
            </div>
        </div>
    </xsl:template>

    <xsl:template match="atributos">
        <div class="atributos">
            <div class="bloque">
                <img src="iconos/Resistencia.png" class="icono" />
                <img src="iconos/{resistencia}.png" alt="{resistencia}" class="valor" />
            </div>
            <div class="bloque">
                <img src="iconos/Energia.png" class="icono" />
                <img src="iconos/{energia}.png" alt="{energia}" class="valor" />
            </div>
            <div class="bloque">
                <img src="iconos/Agilidad.png" class="icono" />
                <img src="iconos/{agilidad}.png" alt="{agilidad}" class="valor" />
            </div>
        </div>
    </xsl:template>

    <xsl:template match="habilidad">
        <div class="habilidad">
            <div class="costo">
                <xsl:choose>
                    <xsl:when test="costo != ''">
                        <xsl:apply-templates select="costo" />
                    </xsl:when>
                    <xsl:otherwise>&#160;</xsl:otherwise>
                </xsl:choose>
            </div>
            <xsl:if test="extra != ''">
                <div class="extra">
                    <xsl:apply-templates select="extra" />
                </div>
            </xsl:if>
            <div class="principal">
                <div class="cabeza">
                    <span class="titulo">
                        <xsl:value-of select="titulo" />
                    </span>
                    <span class="activacion">
                        <xsl:value-of select="activacion" />
                    </span>
                </div>
                <div class="cuerpo">
                    <xsl:choose>
                        <xsl:when test="efecto">
                            <xsl:apply-templates select="efecto" />
                        </xsl:when>
                        <xsl:otherwise>&#160;</xsl:otherwise>
                    </xsl:choose>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="efecto">
        <div class="efecto">
            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="costo">
        <xsl:for-each select="token">
            <div class="fila">
                <xsl:apply-templates select="." />
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="extra">
        <xsl:for-each select="token">
            <div class="fila">
                <xsl:apply-templates select="." />
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="token">
        <div class="token">
            <xsl:if test="@min or @valor or @max">
                <div class="cantidad">
                    <xsl:if test="@min">
                        <img src="iconos/{@min}.png" alt="{@min}" class="min" />
                        <img src="iconos/:.png" alt=":" class="min-sep" />
                    </xsl:if>
                    <xsl:if test="@valor">
                        <img src="iconos/{@valor}.png" alt="{@valor}" class="valor" />
                    </xsl:if>
                    <xsl:if test="@max">
                        <img src="iconos/:.png" alt=":" class="max-sep" />
                        <img src="iconos/{@min}.png" alt="{@min}" class="max" />
                    </xsl:if>
                </div>
            </xsl:if>
            <img src="iconos/{@tipo}.png" alt="{@tipo}" class="tipo" />
        </div>
    </xsl:template>

    <xsl:template match="imagen">
        <xsl:choose>
            <xsl:when test="@archivo">
                <img src="imagenes/{@archivo}" class="imagen" />
            </xsl:when>
            <xsl:otherwise>
                <img src="imagenes/Fondo.png" class="imagen" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="metadatos">
        <span class="temporada">#<xsl:value-of select="temporada/@numero" />&#160;<xsl:value-of select="temporada" /></span>
        <span class="expancion">#<xsl:value-of select="expancion/@numero" />&#160;<xsl:value-of select="expancion" /></span>
        <span class="serial"><xsl:value-of select="/carta/@tipo" />&#160;#<xsl:value-of select="serial" />–<xsl:value-of select="vercion" /></span>
    </xsl:template>

</xsl:stylesheet>
