#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" generador.py

Script que lee un archivo XML representado una carta de juego de TE,
usa una hoja XSLT para crear un HTML y una hoja CSS la cual es exportada
a PDF o imagen
"""

from glob import glob
from os import path, listdir, remove
from logging import basicConfig, getLogger, DEBUG
import traceback

from pathlib import Path
from importlib import resources

from weasyprint import HTML, CSS
from lxml.etree import parse, XSLT, DTD, tostring, XMLParser, Error

basicConfig(filename="registro.log", filemode="w", level=DEBUG)
REG = getLogger(__name__)

# Carpetas de recursos
CARPETA_BASE = path.abspath(path.dirname(__file__))
CARPETA_ESTILOS = path.join(CARPETA_BASE, "estilos")
CARPETA_IMAGENES = path.join(CARPETA_BASE, "imagenes")
CARPETA_CARTAS = path.join(CARPETA_BASE, "cartas")
CARPETA_PNG = path.join(CARPETA_BASE, '..', "png")
CARPETA_HTML = path.join(CARPETA_BASE, '..', "html")
# Hojas de estilo
with open(path.join(CARPETA_ESTILOS, "esquema.dtd"), "r") as archivo:
    ARCHIVO_DTD = DTD(archivo)
ARCHIVO_XSLT = XSLT(parse(path.join(CARPETA_ESTILOS, "estandar.xsl")))
ARCHIVO_CSS = CSS(filename=path.join(CARPETA_ESTILOS, "estandar.css"))


def main(parser, dtd, xslt, css):
    """Funcion principal

    Va por todos los archivos en la carpeta y los transforma
    """
    cartas = glob(path.join(CARPETA_CARTAS, "*.xml"))
    total = len(cartas)
    REG.info("Iniciando transformacion de %s cartas", total)
    for carta in cartas:
        REG.info(
            "Transformando %s de %s: %s",
            cartas.index(carta) + 1, total, carta)
        try:
            xml = parse(carta, parser)
        except Error:
            error = traceback.format_exc()
            REG.critical("Error de de Parceo: %s", error)
        if not dtd.validate(xml):
            REG.info("carta %s no cumple con el esquema", carta)
            REG.warning(dtd.error_REG.filter_from_errors()[0])
        if len(xml.xpath("/carta/nombre")[0].text) > 18:
            REG.warning("carta %s tiene un mombre demasiado largo", carta)
        transformar(xml, xslt, css)
    REG.info("Archivos creados")
    return 0


def transformar(xml, xslt, css):
    """Procesa un archivo xml, usando una hoja XSLT Y CSS crea un imagen PNG"""
    nombre = "{}-{}".format(
        xml.xpath("//nombre")[0].text,
        xml.xpath("//vercion")[0].text,
    )
    carta = xslt(xml, **PARAMETROS)
    html = HTML(string=str(tostring(carta)))
    with open(path.join(CARPETA_HTML, "{}.html".format(nombre)),
              "w") as documento:
        documento.write(str(tostring(carta), "utf-8"))
    html.write_pdf(
        target=path.join(CARPETA_PNG, "{}.pdf".format(nombre)),
        stylesheets=[
            css,
        ],
        # resolution=600,
    )

def execute():
    # Borrar archivos viejos
    for carpeta in (CARPETA_PNG, CARPETA_HTML):
        for archivo in listdir(carpeta):
            remove(path.join(carpeta, archivo))
    # Parametros de transformacion
    PARAMETROS = {
        "base": "'{}'".format(CARPETA_BASE),
    }
    #TODO Desactivar validacion hasta que termine un DTD valido
    PARSEADOR = XMLParser(dtd_validation=False)
    main(PARSEADOR, ARCHIVO_DTD, ARCHIVO_XSLT, ARCHIVO_CSS)


def main():
    
    for i in resources.contents(__package__):
        print(i)
        with resources.path(__package__, i) as p:
            print(p)

# Ejecutar el programa inmediatamente
if __name__ == "__main__":
    main()
